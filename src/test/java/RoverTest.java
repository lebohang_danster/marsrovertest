import org.junit.Assert;
import org.junit.Test;

/**
 * Created by apple on 3/7/17.
 */
public class RoverTest {

    @Test
    public void testRoverYPositionChangesToTwoWhenMovingNorthFromOne() {
        Rover rover = new Rover(1, 1, "N");
        rover.executeCommand("M");

        Assert.assertEquals(1, rover.getXCoordinate());
        Assert.assertEquals(2, rover.getYCoordinate());
        Assert.assertEquals("N", rover.getOrientation());
    }

    @Test
    public void testRoverYPositionChangesToTwoWhenMovingSouthFromThree()
    {
        Rover rover = new Rover (3,2,"S");
        rover.executeCommand("M");

        Assert.assertEquals(2, rover.getXCoordinate());
        Assert.assertEquals(2, rover.getYCoordinate());
        Assert.assertEquals("S", rover.getOrientation());
    }

    @Test
    public void testRoverXPositionChangesToThreeWhenMovingEastFromTwo()
    {
        Rover rover = new Rover(2,2,"E");
        rover.executeCommand("M");

        Assert.assertEquals(3, rover.getXCoordinate());
        Assert.assertEquals(2, rover.getYCoordinate());
        Assert.assertEquals("E", rover.getOrientation());
    }

    @Test
    public void testRoverXPositionChangesToOneWhenMovingWestFromTwo()
    {
        Rover rover = new Rover(2,2,"W");
        rover.executeCommand("M");

        Assert.assertEquals(1, rover.getXCoordinate());
        Assert.assertEquals(2, rover.getYCoordinate());
        Assert.assertEquals("W", rover.getOrientation());
    }

    @Test
    public void testRoverChangesOrientationToEastWhenTurningRightFromNorth() {
        Rover rover = new Rover(2, 2, "N");
        rover.executeCommand("R");

        Assert.assertEquals(2, rover.getXCoordinate());
        Assert.assertEquals(2, rover.getYCoordinate());
        Assert.assertEquals("E", rover.getOrientation());
    }

}
