/**
 * Created by apple on 3/7/17.
 */
public class Coordinate {

    private final int y;
    private final int x;
    private Orientation orientation;

    public Coordinate(int y, int x, Orientation orientation) {
        this.y = y;
        this.x = x;
        this.orientation = orientation;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public Orientation getOrientation() {
        return orientation;
    }
}
