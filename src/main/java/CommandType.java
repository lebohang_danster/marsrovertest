/**
 * Created by apple on 3/7/17.
 */
public enum CommandType {
    L {
        @Override
        public Coordinate execute(Coordinate coordinate) {
            return new Coordinate(coordinate.getY(), coordinate.getX(), coordinate.getOrientation().TurnLeft());
        }
    },
    R {
        @Override
        public Coordinate execute(Coordinate coordinate) {
            return new Coordinate(coordinate.getY(), coordinate.getX(), coordinate.getOrientation().TurnRight());
        }
    },
    M {
        @Override
        public Coordinate execute(Coordinate coordinate) {
            return coordinate.getOrientation().Move(coordinate);
        }
    };

    public abstract Coordinate execute(Coordinate coordinate);
}
