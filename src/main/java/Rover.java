/**
 * Created by apple on 3/7/17.
 */
public class Rover {

    private Coordinate coordinate;

    public Rover(int y, int x, String orientation) {
        coordinate = new Coordinate(y, x, Orientation.valueOf(orientation));
    }

    public void executeCommand(String commands) {
        int length = commands.length();
        for (int i = 0; i < length; i++) {
            char cmd = commands.charAt(i);
            CommandType command = CommandType.valueOf(String.valueOf(cmd));
            coordinate = command.execute(coordinate);
        }

    }

    public int getXCoordinate() {
        return coordinate.getX();
    }

    public int getYCoordinate() {
        return coordinate.getY();
    }

    public String getOrientation() {
        return coordinate.getOrientation().toString();
    }
}
