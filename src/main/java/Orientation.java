/**
 * Created by apple on 3/7/17.
 */
public enum Orientation {
    N {
        @Override
        public Orientation TurnRight() {
            return E;
        }

        @Override
        public Orientation TurnLeft() {
            return W;
        }

        @Override
        public Coordinate Move(Coordinate coordinate) {
            return new Coordinate(coordinate.getY() + 1, coordinate.getX(), coordinate.getOrientation());
        }
    },
    S {
        @Override
        public Orientation TurnRight() {
            return W;
        }

        @Override
        public Orientation TurnLeft() {
            return E;
        }

        @Override
        public Coordinate Move(Coordinate coordinate) {
            return new Coordinate(coordinate.getY() - 1, coordinate.getX(), coordinate.getOrientation());
        }
    },
    E {
        @Override
        public Orientation TurnRight() {
            return S;
        }

        @Override
        public Orientation TurnLeft() {
            return N;
        }

        @Override
        public Coordinate Move(Coordinate coordinate) {
            return new Coordinate(coordinate.getY(), coordinate.getX() + 1, coordinate.getOrientation());
        }
    },
    W {
        @Override
        public Orientation TurnRight() {
            return N;
        }

        @Override
        public Orientation TurnLeft() {
            return S;
        }

        public Coordinate Move(Coordinate coordinate) {
            return new Coordinate(coordinate.getY(), coordinate.getX() - 1, coordinate.getOrientation());
        }
    };

    public abstract Orientation TurnRight();
    public abstract Orientation TurnLeft();
    public abstract Coordinate Move(Coordinate coordinate);
}
